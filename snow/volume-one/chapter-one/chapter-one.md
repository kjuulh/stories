# Chapter One

## The Meeting

Staring at his screens David sighed. He had just picked up a fresh cup of coffee, but it didn't seem to help with his problem. The bug had weirdly enough, not solved itself while he was at break. To be fair, it was to be expected, as software-related problems tend to not solve themselves over time. But a man could hope, thought David.

He had worked at his current firm for a little over 2 years, and was barely 24 years old. He had quickly fallen into a routine, and he was already beginning to feel like an old man. He had never held much passion for his job as a Software Developer, but it made payed the bills even if it was a minimal wage job.

In this day and age, Software developers were the new blue-collar workers. Manual labour had been replace long ago, now programming was one of the last 'manual jobs'. David has once dreamed of a more prestigious career, but it wouldn't be. He thought that it was a shame that he didn't have an ounce of creativity. It just wasn't his thing. He had always wondered if he was born in a different time. In a time where technology wasn't a thing, where mental and physical thoughness was key. He had always been good a fighting thanks to his 'upbringing' but he really didn't fit in this day and age.

But he had promised his shrink, that he wouldn't even try to pursue a career in martial arts. It was one of the reasons he was left of the hook in his past. Also he wouldn't want to return to what he once was, he had already worked so hard, to just maintain his current situation.

David heard a couple of rapid steps behind him, he turned around in his chair as he always did when she arrived. He could already smell the cheap purfume in the air, it clung to everything, for something so cheap it was really persistent. Her smell would linger long after she was gone, just to remind him of her presence. His heart tighened as the steps grew louder.

Just over at the entrance to his cupicle, a beast of a woman appeared. Around 1.6 meters tall, with flaming red hair, she probably weigheded about the same as him, and he was about 1.9 meters tall, and built like a bodybuilder. Her temper just as bad as her perfume.

"You know David, for someone built like you, one would think that you were dedicated. But when I read the code from you, I get so disappointed. You are behind schedule on your deliveries, your reviews has been less than satisfactory. You better step it up, or I am going to bring it up to management" The woman said, then turned a left.

The nerve, she didn't even give him a chance to speak. But it didn't matter. He could only focus on his job. He quickly turned around to continue his job. He was behind schedule after all.

Quickly after she left, a hand poked his shoulder, it was Terry. Terry was under 30 and looked like the steroetipical programmer that he was. Dark brown heir, around 1.7 meters tall wearing the official dress code suit, with a lanky build, mostly caused by his nervous and shy disposition.

Terry always seemed to show up after Ruby had been at his cubicle. It was probably because Terry was one of those people that complained. And he liked to have accomplises. When you are a worker and you don't like your job, but it forced to do it because of your circumstancese then there are usually two outcomes, either you complain or you just work. Terry was the former, David the latter.

There was of course the third option, to resign. But that wasn't really a choice in this day and age. Those with the prividge of a job, were in for a wild, but tragic ride. Without credits, you would be homeless. And homeless people were some of the easiest people to target for all kinds of shady deals.

It was terrifying what kinds of drugs were being conconted, once, and you were hooked for life. Once there were drugs like cocain or heroin. That stuff was childsplay. That wasn't to say that addict that had gone clean weren't tough, no, they were probably some of the people with the toughest mindsets. Today, you didn't have the option to quit, not unless you were filthy rich or had a steady supply of the stuff. Getting drugged was nearly a death sentence.

David had worked with those previously and he didn't like the odds, so he just worked his ass of. It wasn't like he had never done something he didn't want before. It was all a question of survival, he didn't go down then, and he wouldn't now. Actually David could've chosen the fourth option, to rebel. It certainly wouldn't end well for Ruby. But he had already worked so hard to maintain his cover.

Terry sneered like he always did: "That bitch has been after us all week, I really should resign, I can't stand this place anymore. Someone should tell her to mind her own business, it isn't like she knows what she is talking about".

David could only agree, but he knew that it was just words, Terry could talk his mouth off, but he would never do anything. They both knew the risks of talking to ones superior, getting fired was not an option.

David calmed mentioned "Shhh, you don't want her to come back, go back to your cubicle, I've tons of work I need to get done, and I don't have time to complain".

Some might be in it for the money, even if it was a small amount, he could easily live by it, he had learnt at an early age to be frugal. No, he was in it for being 'normal'. See David possessed something that few people did, it was a wonder that he could even work a normal job. As such he had to stay out of the way, and not fall into his old path. It wouldn't do him or anybody else any good, if that were to happen.

David sighed as he shut down his computer and pocketed the portable the small computer. Another day, another day sober. His current streak was 962. When he walked to the elevator, he was met with an annoying figure, Terry. When Terry wasn't complaining, to david, he was trying to get him out to drink. David figured, it must be because he figured that someone with his figure would be popular with the ladies, but he wouldn't know. He never did drink.

"Come on David, what do you say. I'll give the first round! Just like the old times, just me and you" said Terry with a smile. Terry figured he must've hit his head, because there weren't any old times, because he had never been out to drink with him, ever.

David just glanced at the guy and didn't even bother. He had once tried to say no, but the man never gave up. He must've really been desperate. The two guys entered the company elevator.

It was a pretty awkward atmosphere in there, but David didn't budge, no small talk, no nothing. Terry would begin talking at the slightest reaction, so he just kept calm and thought about his apartment, and what he would do for the evening.

It was a short ride, elevators were pretty fast, and David quickly left, when the doors sleed open. Freedom he thought. As he stood outside the building, but was quickly reminded, that living in the city that was marked as a danger zone because of pollution wasn't nice. Nothing like the smell of burn tires and ammonia, he thought.

Being in a city was sensory overload, and not the good kind. For his ears the noice was defeaning, voices booming all around, only to be briefly suppresed, by sirens or hoonking horns. His nose was assaulted by the smell of hot garbage. For his eyes dancing neon light all around, which destroyed the night sky and filled his entire view. Like everything, it had only gotten more intrusive as time had progressed, you couldn't go anywhere without being overwhelmed by commercials for a new shampoo, that promised longer life, or vitamins curing cancer. It was probably all fake and shady. But in a capitalistic world it was to be expected.

He walked down the much too crowded street, among all the other people, he felt like a fish in a barrel. He walked and walked, but he felt he didn't get anywhere, it wasn't too uncommon for someone to give him an elbow in the ribs, or run straight into him. However, he was usually straight as a pillar, he had seen so much worse.

It was only when something hit him really hard in the back, and stumpled forward, Before he fell to the sidewalk, to get trampled by the masses. He pushed forward in a quick motion, it seemed that he just stopped in midair for a brief pause, to only regain his footing, with a steady pose. The knees bent, and the arms brought to his chin in a boxers position, ready to either take a hit or give one.

It proved to be unnecessary, what had hit him like a brick, was a petite girl, with long auburn hair. She was probably the most beautiful and delicate girl he had ever seen. Luckily he was able grap her before she was trampled. Around them people had begun creating a circle, no one wanted to be a part of a fight. The police of this time, were basically glorified attack dogs. None wanted any business with them.

The girl had fallen unconcious, probably when she collided with his solid back. David was unusually calm for what he should've been. It wasn't that he had been in a situation like this before, but it wouldn't be beyond his old bosses to do a shady tactic.

He could picture in his mind, that the unconcious girl in his arms, turning and stabing him in the eye with a dagger or a needle. In fact he could already be poisoned right now.

Luckily, he had been rather calm as off late, and quickly recognised that this might've all been a coincidence. Unlikely, but not unheard of. He concluded that he should take the girl to his apartment. He couldn't take her to the hospital, as it would look like he assaulted the girl, and he knew better than to trust the other people on the sidewalk. He had plenty of enemies, he figured that a few might be able to "pursuade" some witnesses to tell the "truth".

In a princess carry he took the girl down the street. And people quickly parted, it didn't take more than a moment for the street to once again become busy. Nothing less than a grenade would disrupt this amount of traffic.

Walking up to a rather dirty looking skyscraper in the northeast part of the city. He sighed "home sweet home". The huge concrete skyscraper looked like it could topple any second. In fact broken tiles were on the ground surrounded by dried blood. There was a reason that rent was so cheap in the area. Again, his frugality might've been a blessing or a curse.

The lobby was only illuminated by a few blinking flourecent lights, it would have been straight out of a horror story, if a few large windows hadn't let in enough sunlight to illuminate the room. But, it was empty nonetheless. If not for the honking car sounds in the background, he could easily imagine a western tune playing in the background and a dustball rolling down the large room.

It was fair to say that the building probably housed more than a few druglords, gangsters and other scum of society. David however, was confident that he wouldn't be in much danger. He had enemies that would make these piss their pants in fear.

Continuing to yet another elevator, David wondered if it was gonna fall down while he was in it, it made horrible screeching sounds as it rose throught the levels. He had once made a silent bet with himself, if it would crash with him, or another person. It would someday, hopefully not with him in it.

But right now he was especially not hoping for it, he'd rather not take the innocent girl to her death. She was still in his arms, light as a feather. He actually wondered if she was awake, but didn't dare to open her eyes. He didn't really care. He would just let her off, when she decided to wake up.

Exiting at the elevator and going through the hallway to his apartment, at the far end of hallway, he could hear several arguments, fights and general loudsness from the different rooms. It really was a place that one wouldn't set foot in unless they absolutely had to.

David checked his door to see if anyone had broken in before he entered. It was a daily habit of his, so that he could call the polica if there was any evidence, he wouldn't be able to collect insurance otherwise.

He sighned as he flashed his keycard over the mechanical lock, archaic but effective. Most new systems used implanted biochips in the person, most had them, he didn't. Usually when a baby was born, a chip was implantet so that the system would recognice the person, even if the person was damaged beyond recognition. Well, it was also handy to apprehend dangerous criminals. If implanted with the chip it would spread and implant itself in nearly all the tissue, next to impossible to remove. He didn't have it for several reasons, one being that he wasn't born in a hospital and didn't hold a record.

In fact, he had faked his ID to get a job. In this day, being a programmer was quite easy, most received an education in programming from an early age, so a fresh highschool student could do his job in his sleep. He didn't have that kind of education, so it was quite a struggle for him.

Entering his room, a clean, but stale scent hit nose. It wasn't really possible to air out in the apartments, mostly because it was hazardous outside. Second, because his apartment lacked windows, instead it was replace with wall to wall flatscreens, projecting a clear skyline, obviously it was fake. As you couldn't see a meter outside in this altitude.

The air cleared up immediatly, when he entered, in fact the lights turned on and a record of his favorite band started playing. Queen, nothing like a good classic.

His room was quite modern, even if it showed signs of being underused, the kitchen lacked utensils and the hardware didn't look like it had much use. The only thing that had any real use, was a mat in the middle of the room and a bookshelf that was filled to the brim with old books.

He also had a comfy sofa, it looked cheap, but it served more as a decoration than anything. Finally, it would get some use. David let down the girl gently in the sofa.

David took a second look at her, he had noticed the softness of her skin, it was like she didn't have a single real day of work on her hands in her whole life. But her curves and symmetrical face caught his attention far more. She had a nice face, with a calm smile on her lips, a pepite nose with a few strands of her auburn desphellwed hair over her face.

She didn't have the largest breasts, but she had a slim waist and wide hips. He wasn't quite sure, but it looked like she had fairly long legs as well. But the thing that he focused on was the hands. She had such dainty and fragile hands, that he just wanted to caress and store them away. Like a precious treasure to be burried and hidden away.

It wasn't normal for him to have thougths like these. He wasn't used to it, then again it was rare to see a woman like this. In fact, he couldn't remember a single woman he had ever seen on this level. But, he wasn't intimiated, he never was.

After a few minutes of her laying down, her eyelashes began to flutter, and her eyes very quickly shot wide open. She quickly she of the ground, knocking the sofa over, her tumbing over it's back and smashing her head against the wall.

David grinned at the pathetic sight. He got up to give her a hand, as she was hidden by the fallen over sofa. Cautiously he leaned over to see the situation.

A loud smack rang in the room, as a red imprint began appearing on his cheek. The girl staring at him with wide open eyes, and a threatening glare. David didn't really care about the slap, although he had gotten a red cheek, to him it was to the level of getting hit with a pillow. he was also fairly sure that she was in the right, he had afterall "kidnapped" her. Most would probably call and ambulance or take her to the hospital. He must've seemed like a creap.

"Where am I and who are you and why have you taken me?" shouted the girl. She took a pathetic fighting stance, but she was ready to fight for her life.

David shook his head and answered "I am sorry for this unfortunate situation, you are in my apartment in block eastwing 3-4-6, I am David Jensen, but you can just call me David. You nearly knocked me over on the streets, but you fell unconcious, I would've taken you to the hospital, but for certain reasons I can't take you there".

The girl looked doubtful, and David didn't blame her, he must seem like a lunatic. "But why did you take me?" the girl said, while she seemed to have let her guard down a little.

David answered "I have a trust issue with most stranger, I certainly couldn't leave an unconcious girl on the streets. Can you allow me to set the sofa up again, so that we can have a proper conversation. My word might not mean much to you, but I promise to not hurt you."

The girl nooded after a brief pause, her complexion changed, to a slightly aloof persona. A mile away from her scared, but steadfast expression previously.

David raised the sofa back up, facing towards the large flatscreen. They both sat down, facing sideways towards each other.

David started out. "I am sorry, that we got off the wrong foot. I don't usually talk to other people, so conversations can seem a bit awkward to me. Let's start with an introduction, I've already given you my name, it would make it easier, if I could have yours."

"Its alright, I am not too good at that myself, and sorry about the slap. I am Alice by the way. It's nice to meet you and thanks for the save before." said Alice with an outstretched hand.

David grapped the hand and shook it. "Nice to meet you Alice, Its alright with the slap, you shouldn't feel bad about it. I wouldn't suggest you go about flipping peoples sofas over though".

Alice quickly began figgedting, breaking her demeanor. David asked, "Why were you running before? You must know that it can be dangerous to run in the streets." She replied with a quick yelp and rose from the sofa, she had completely forgotten that she was running from 'them'. With a frantic rush she ran to the door. Luckily a flatscreen was projected on the door, so the entire hallway was visible from the inside.

What the image projected was two large men, putting regular body builders to shame. They calmly walked to the door, it seemed that they've followed them here. Hopefully they didn't know which room, but because they knew the floor. It was unlikely.

The one on the right was a large black man, with a big mustace and bald head. It seemed that he had a large barcode tattoed on the left side of his bald head. He didn't look too bright, but strong and arrogant enough to make up for it. The one of the left was a shorter white man, with a sadistic grin plastered on his scarred face. The grin maken worse by was inconsistent lack of teeth. He wore a regular army cut, and was the only one carrying a weapon, what looked like some kind of machine gun.

To carry a weapon was highly illegal, but it seemed that he didn't care even in the slightest, in fact he waved it around like a flag.

David was a little startled by Alice's reacting but quickly picked up his bearings when he saw the gorillas in the hallway, they were probably 50 centimeters taller than him. He sighed, he always go into trouble. And he had worked so hard to keep his streak going.

He had a very uncommon habit. When he got angry, he go really angry and it usually resulted in someone getting severely hurt or duying. It isn't very fitting for a member of society to go around killing people because they spilled coffee on his shirt.

To him anger was a drug, it would pull him in and it would make him seek fights. He stopped, because one day he would meet an opponent he couldn't face or was overwhelmed. He didn't exacly feel like duying at a tender age of 24, before this moment he didn't have anything to fight for either. He didn't really fight for himself, it was always at the behest of someone else. Because of his condition fights would be irresistle in his bloodcrazed state.

He rose from the sofa, walking to his closet, and picked up a set of brass knuckles, it was simple yet effective tool, it was in his top 5 of favorite tools. It allowed a personal connection to his victims that he wouldn't get with a gun, a sword or really any other tool than what he could wear on his fists, there were exceptions of course, as knuckles werent suited for everything.

David sighed, he had gotten rid of most of his tools, knuckles weren't really suited for this situation either, but he would have to make do. The two guys were now outside his door, and he wondered how they knew he was here. He was usually very cautious with people following him, and he hadn't sensed any.

Alice tried to keep quiet, but with visible panic she ran to David. "We must escape, I am so sorry for bringing you into my trouble, but I doubt they would take me without killing you." she said with tears in her eyes. He didn't think about escaping, instead he taught about how cute she looked when crying.They couldn't really escape either, there weren't any windows and the fire escape was behind the gorillas.

He didn't know why, but he smiled, which he rarely did, and patted her head. It was rare for someone to show concern for him, he had been used his whole life, and if there were one thing he hated, it was being used. If he every found out he was cheated, there would be hell to pay. But he hoped for a miracle.

They were interruped with an the doorbell being ringed. David walked over to the door, and began talking, "Hello, what can I help you with?".

The white man replied, "We come on behalf of the Red Moon, you've got something of ours, and we would like it back. You've got 10 seconds to open the door and hand her to us, or we will take her with force" David sighed, he could see them, but they couldn't him. So they couldn't see that he was unaturally calm for being in a situtation like this. He equiped his knuckles, and swiped the door handle to unlock the door. The door wouldn't handle anyone who was persistent enough anyway.

David readied his stance bent his knees, and pilled his right hand backwards, he tightened his fist around the knuckle and swung the door open. The men weren't prepared for an onslaught, so they were caught of guard. With a flash, he unleashed all the stored kinetic energy in his stance and thrust his fist forwards. Unprepared the white man took the fist and thereby the knuckle right in the nose, making it cave in. He was dead on the spot. Because of the momentum the white man fell over backwards, the head smashing into the cold metallic tiles. His head didn't break, but it didn't matter, because he was dead the second Davids fist impacted with his face.

The black man quickly jumped back, and he took a fighting stance. With a slighly lowered pose, and his arms raised in a boxers stance. Alice was visible shook in the background, she had been chased all over the city these past months by these guys. She had hidden from them; in alley ways, turned herself into the police, stolen cars. All had been unsuccessful, the Red Moon was one of the toughest and most merciless gangs, they were filled with bio-engineered gangsters, ready to tear other people to shreds. The white mans with the iconic name of Scarface and the Black man with the weird name Log. Had been the ones chasing her, the had dismantled entire families in search of her, they had killed police officers, which is no small feat. She had checked herself over again and again, but they were able to track her, nearly perfectly and she couldn't find any tracking devices.

When she looked at David, she shook in fear. She had never seen anything like it, it was like standing on the edge of death, she stood at the back of him, so she couldn't see his face. But if she did she would probably be even more panicked. David wore a wide smide, and the whites in his eyes had been replace with a red bubbling liquid that slushed around.

He was the spitting image of a devil. With a satistisk grin he began walking to the black man.

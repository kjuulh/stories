Volume One

Tile: Deviation of what should've been.

Featuring:

Main: David Jensen

Romantic Interest:

- Alice
- Elana

David works a menial job as a programmer, currently working for a larger consultancy firm. His job is primarily support a custom build CMS system for the firms offerings.

While 'friends' around him dwell in their normalcy, he is doped up on medication to take away his anxiety, and keep It closed.

One day while going home, in his monochrome world, a brief splash of paint ran into him, tumbling him over. They get up, and a brief pause happens between them. The girl says she is sorry and asks for help, but he denies, he just want to stay out of trouble. One minute after, he hears gunshots in the background, he runs towards the sound. On the ground the girl lies duying, he quickly calls the ambulance, and tries to save the girls life, no robber or attacker in sight.

He goes with her to the hospital, but on the way to the hospital, they have a brief talk, before she fades.

This really, hit him hard, rattling in his 'cage'. Panic attack and violent breakout proceed to happen in the following weeks. He has never felt like this, he has seen his fair share of damage and pain. But something about this seemed to tuck at his heartstring, something that had only happened when he was a 'child'.

To combat his mental instability he meets doctors and pschiatrists, but they all want to give him medicine to forget, it doesn't work, it never has. Suffering from depression he tried to take his life, only to really struggle to comprehend what the girls last words were.

As he prepared to end his worthless life, a lucky save, saves him from death. And he is given a mission of redemption. Finally he comprehended the words she said, it was a long-shoot, but for a chance of redemption he decides to go.

He encouters Elana a blond on his journey, a hitchhiker, who should've only stayed with him for the ride. Fate would have it that she was a prostitute on the run. With the russian mafia on her tail. Now in trouble David is faced with a choice of abandoning the girl or saving her as he should've with Alice.

They continue their journey, and Davids It is triggered multiple times with violence. Elana fearing the man, who is her only chance of survival stays with him. They make it to his family home, where his parents and sister is buried.

Elana is shocked to see the name Alice Jensen on Davids sister gravestone. She quickly backs away to run. But David doesn't seem to notice the coincidence and the likeness to his sister and Alice's profile. David talks to Elana, and his cloudy mind is finally lifted, and goes into panic. He knows that his sister has been dead for years.

But mistake they notice the words etched to his sisters grave. They follow the trail and finds a book. The book of Wonderland. Reading the etherial book, David is afflicted with chaos. Which at first doesn't seem to do anything, but later on reveals Alice, his sister, as a ghostly figure.

Traveling back in shock, Davids notices only two gravestones, his sisters seemed to have faded away like it was never there.

Out of his mind he leaves, leaving Elana all alone. He didn't want to end her also, his fragile mind can't handle this stress.

He accelerates and drives into a tree only for him to be saved by Alice. she shackles him in mistical energies talking to him about what has happened and why she tricked him. She needed to alter his fate, to unlock what he was, or what he could become. She reveals that she is the OrbWeaver, a special kind of Oracle and guide, and that the world is not what it seems to be. A grave danger await, and only she can guide him to 'his' survival.

As a coincidence, the night sky is illuminated with flames, as large metallic object deceand from space. Some crashing, some does not. The world would be changed, forever.

Deciding to find out what happens, David checks the internet, it seemed some large cities where hit by the meteors, but to what seemed to be a pure coincidence. The party of three decide to move towards a small meteor that nearly hits them. Alice convinces them to investigate rather that flee, as she had predicted everything so far, it would be unwise to argue.

The metallic shape, began burning itself into the grounds, creating molten lava from the rocks around it. Rising from the smoke and flames, a large metallic tower rose. From the newly created entrance, regular goblins from a fantasy game rushed forward. Meeting them in combat, David quickly dismantled them with ruthless effeciency. Elana wants to sneak away, but also doesn't want to stay alone in the forest, with all these dangers, follows.

Entering the tower, they are transported to a cavern, illuminated by the burning sun. Curious, as it was night when they entered the tower. Quickly rushing back, they notice that they are only faced with regular dirt walls.
